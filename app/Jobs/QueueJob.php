<?php

namespace App\Jobs;

use App\Services\ApiDataService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class QueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $start_datetime;
    protected $from;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($start_datetime, $from = 0)
    {
        $this->start_datetime = $start_datetime;
        $this->from = $from;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ApiDataService $data_service)
    {
        $response = $data_service->getApiData($this->start_datetime, $this->from);

        $next_page = $data_service->isNextPage($response);

        foreach(array_chunk($response, 1000) as $value) {
            $job = (new BetlogJob($value))->onQueue("betlog");
            dispatch($job);
        }

        if ($next_page) {
            $this->from += 10000;
            $job = (new QueueJob($this->start_datetime, $this->from))->onQueue("page");
            dispatch($job);
        }
    }
}
